﻿namespace Minecraft.Crafting.Api.Models
{
    public class Item
    {
        public Item()
        {
            EnchantCategories = new List<string>();
            RepairWith = new List<string>();
        }

        public DateTime CreatedDate { get; set; }
        public string DisplayName { get; set; }
        public List<string> EnchantCategories { get; set; }
        public int Id { get; set; }
        public int MaxDurability { get; set; }
        public string Name { get; set; }
        public List<string> RepairWith { get; set; }
        public int StackSize { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}