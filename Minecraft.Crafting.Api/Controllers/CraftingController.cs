namespace Minecraft.Crafting.Api.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Razor.Infrastructure;
    using Minecraft.Crafting.Api.Models;
    using ProjetBlazor.Factories;
    using ProjetBlazor.Modeles;
    using System.Collections;
    using System.Data;
    using System.Text.Json;
    using System.Text.Json.Serialization;

    [ApiController]
    [Route("api/[controller]")]
    public class CraftingController : ControllerBase
    {
        public IWebHostEnvironment WebHostEnvironment { get; set; }

        private readonly JsonSerializerOptions _jsonSerializerOptions = new()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            WriteIndented = true,
            DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault
        };


        [HttpPost]
        [Route("")]
        public Task Add(JsonElement json)
        {

            MusiqueModel musiqueModel = JsonSerializer.Deserialize<MusiqueModel>(json);
            Musique musique = MusiqueFactory.Create(musiqueModel);

            var data = JsonSerializer.Deserialize<List<Musique>>(System.IO.File.ReadAllText("Data/musique.json"), _jsonSerializerOptions);

            int tmp = data.LastOrDefault().id;
            musique.id = tmp + 1;
            data.Add(musique);

            System.IO.File.WriteAllText("Data/musique.json", JsonSerializer.Serialize(data, _jsonSerializerOptions));

            //Sauvegarder image et song
            //image
            /*
            string fileName = @$"{Environment.CurrentDirectory}/wwwroot/images/" + musique.imageName;
            using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                byte[] img = musiqueModel.image;
                fs.Write(img, 0, img.Length);
            }
            
            using (var memoryStream = new MemoryStream())
            {
                string fileName = @$"{Environment.CurrentDirectory}/../wwwroot/images/" + musique.imageName;

                FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);

                byte[] img = musiqueModel.image;

                fs.Write(img, 0, img.Length);
                fs.Close();
            }*/
            //song
            /*
            using (var memoryStream = new MemoryStream())
            {
                string fileName2 = @$"{Environment.CurrentDirectory}/wwwroot/musiques/" + musique.songName;

                FileStream fs = new FileStream(fileName2, FileMode.Create, FileAccess.Write, FileShare.None);

                byte[] song = musiqueModel.song;

                fs.Write(song, 0, song.Length);
                fs.Close();
            }*/
            return Task.CompletedTask;
        }

        [HttpGet]
        [Route("count")]
        public Task<int> Count()
        {
            var data = JsonSerializer.Deserialize<List<Musique>>(System.IO.File.ReadAllText("Data/musique.json"), _jsonSerializerOptions);

            return Task.FromResult(data.Count());
        }

        [HttpDelete]
        [Route("{id}")]
        public Task Delete(int id)
        {
            var data = JsonSerializer.Deserialize<List<Musique>>(System.IO.File.ReadAllText("Data/musique.json"), _jsonSerializerOptions);

            var item = data?.FirstOrDefault(w => w.id == id);

            if (item == null)
            {
                throw new Exception($"Unable to found the item with ID: {id}");
            }

            data.Remove(item);

            System.IO.File.WriteAllText("Data/musique.json", JsonSerializer.Serialize(data, _jsonSerializerOptions));

            return Task.CompletedTask;
        }

        [HttpGet]
        [Route("{id}")]
        public Task<Musique> GetById(int id)
        {
            var data = JsonSerializer.Deserialize<List<Musique>>(System.IO.File.ReadAllText("Data/musique.json"), _jsonSerializerOptions);

            var item = data?.FirstOrDefault(w => w.id == id);

            if (item == null)
            {
                throw new Exception($"Unable to found the item with ID: {id}");
            }

            return Task.FromResult(item);
        }


        [HttpGet]
        [Route("")]
        public Task<List<Musique>> List(int currentPage, int pageSize)
        {
            var data = JsonSerializer.Deserialize<List<Musique>>(System.IO.File.ReadAllText("Data/musique.json"), _jsonSerializerOptions);

            return Task.FromResult(data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList());
        }

        [HttpGet]
        [Route("listAll")]
        public Task<List<Musique>> ListAll()
        {
            var data = JsonSerializer.Deserialize<List<Musique>>(System.IO.File.ReadAllText("Data/musique.json"), _jsonSerializerOptions);

            return Task.FromResult(data);
        }




        [HttpPost]
        [Route("update")]
        public Task Update(JsonElement json)
        {
            MusiqueModel musiqueModel = JsonSerializer.Deserialize<MusiqueModel>(json);
            Musique musique = MusiqueFactory.Create(musiqueModel);

            var data = JsonSerializer.Deserialize<List<Musique>>(System.IO.File.ReadAllText("Data/musique.json"), _jsonSerializerOptions);

            var itemOriginal = data?.FirstOrDefault(w => w.id == musique.id);

            if (itemOriginal == null)
            {
                throw new Exception($"Unable to found the item with ID: {musique.id}");
            }

            itemOriginal.id = musique.id;
            itemOriginal.titre = musique.titre;
            itemOriginal.auteur = musique.auteur;
            itemOriginal.duree = musique.duree;
            itemOriginal.genre = musique.genre;
            itemOriginal.date = musique.date;
            itemOriginal.ImageBase64 = musique.ImageBase64;
            itemOriginal.songName = musique.songName;

            System.IO.File.WriteAllText("Data/musique.json", JsonSerializer.Serialize(data, _jsonSerializerOptions));

            return Task.CompletedTask;
        }

    }
}