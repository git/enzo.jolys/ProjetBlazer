﻿namespace Minecraft.Crafting.Api.Definitions
{
    using System.Text.Json.Serialization;

    public class RecipeInShapeDefinition
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }

        [JsonPropertyName("metadata")]
        public long Metadata { get; set; }
    }
}
