﻿using Minecraft.Crafting.Api.Models;
using ProjetBlazor.Modeles;
using System.Reflection;

namespace ProjetBlazor.Factories
{
    public static class MusiqueFactory
    {

        public static Musique Create(MusiqueModel musique)
        {
            return new Musique
            {
                id = musique.id,
                titre = musique.titre,
                auteur = musique.auteur,
                duree = musique.duree,
                genre = musique.genre,
                date = musique.date,
                ImageBase64 = Convert.ToBase64String(musique.image),
                songName = musique.songName,
            };
        }
    }
}
