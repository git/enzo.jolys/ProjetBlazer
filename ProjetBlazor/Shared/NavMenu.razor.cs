﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using ProjetBlazor.Pages;

namespace ProjetBlazor.Shared
{
    public partial class NavMenu
    {
        [Inject]
        public IStringLocalizer<NavMenu> Localizer { get; set; }
        [Inject]
        public NavigationManager NavigationManager { get; set; }
    }
}
