﻿using Blazored.Modal.Services;
using Blazored.Modal;
using Microsoft.AspNetCore.Components;
using ProjetBlazor.Services;
using ProjetBlazor.Modeles;

namespace ProjetBlazor.Modals
{
    public partial class DeleteConfirmation
    {
        [CascadingParameter]
        public BlazoredModalInstance ModalInstance { get; set; }

        [Inject]
        public IDataService DataService { get; set; }

        [Parameter]
        public int Id { get; set; }

        private Musique musique = new Musique();

        protected override async Task OnInitializedAsync()
        {
            // Get the musique
            musique = await DataService.GetById(Id);
        }

        void ConfirmDelete()
        {
            ModalInstance.CloseAsync(ModalResult.Ok(true));
        }

        void Cancel()
        {
            ModalInstance.CancelAsync();
        }
    }
}
