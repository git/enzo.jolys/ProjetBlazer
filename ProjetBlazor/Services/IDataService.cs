﻿using ProjetBlazor.Modeles;

namespace ProjetBlazor.Services
{
    public interface IDataService
    {
        Task Add(MusiqueModel musique);

        Task<List<Musique>> List(int currentPage, int pageSize);

        Task Delete(int id);

        Task Update(MusiqueModel musique);

        Task<Musique> GetById(int id);

        Task<int> Count();

        Task<List<Musique>> ListAll();

    }
}
