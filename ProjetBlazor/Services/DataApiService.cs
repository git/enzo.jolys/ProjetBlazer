﻿using ProjetBlazor.Factories;
using ProjetBlazor.Modeles;

namespace ProjetBlazor.Services
{
    public class DataApiService : IDataService
    {
        private readonly HttpClient _http;

        public DataApiService(HttpClient http)
        {
            _http = http;
        }

        public async Task Add(MusiqueModel musiqueModel)
        {        
            // Save the data
            await _http.PostAsJsonAsync("https://localhost:7234/api/Crafting",musiqueModel);
        }

        public async Task<int> Count()
        {
            return await _http.GetFromJsonAsync<int>("https://localhost:7234/api/Crafting/count");
        }

        public async Task Delete(int id)
        {
            await _http.DeleteAsync($"https://localhost:7234/api/Crafting/{id}");
        }


        public async Task<Musique> GetById(int id)
        {
            return await _http.GetFromJsonAsync<Musique>($"https://localhost:7234/api/Crafting/{id}");
        }

        public async Task<List<Musique>> List(int currentPage, int pageSize)
        {
            return await _http.GetFromJsonAsync<List<Musique>>($"https://localhost:7234/api/Crafting/?currentPage={currentPage}&pageSize={pageSize}");
        }

        public async Task<List<Musique>> ListAll()
        {
            return await _http.GetFromJsonAsync<List<Musique>>($"https://localhost:7234/api/Crafting/listAll/");
        }

        public async Task Update(MusiqueModel musique)
        {
            // Get the item
            //var item = MusiqueFactory.Create(musique);

            await _http.PostAsJsonAsync($"https://localhost:7234/api/Crafting/update/", musique);
        }

    }
}
