﻿using Microsoft.AspNetCore.Components;
using ProjetBlazor.Services;
using ProjetBlazor.Modeles;
using Microsoft.AspNetCore.Components.Forms;
using ProjetBlazor.Factories;
using Microsoft.Extensions.Localization;
using ProjetBlazor.Shared;

namespace ProjetBlazor.Pages
{
    public partial class Edit
    {
        
        [Parameter]
        public int id { get; set; }

        private MusiqueModel musiqueModele = new MusiqueModel();

        [Inject]
        public IWebHostEnvironment WebHostEnvironment { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IDataService DataService { get; set; }

        [Inject]
        public IStringLocalizer<NavMenu> Localizer { get; set; }

        private async void HandleValidSubmit()
        {
            await DataService.Update(musiqueModele);

            if ( musiqueModele.song != null)
            {
                string fileName = @$"{Environment.CurrentDirectory}/wwwroot/song/" + musiqueModele.songName;

                using (FileStream fs = File.Create(fileName))
                {
                    byte[] info = musiqueModele.song;
                    fs.Write(info, 0, info.Length);
                }
            }           

            NavigationManager.NavigateTo("");
        }


        private async Task LoadImage(InputFileChangeEventArgs e)
        {
            // Set the content of the image to the model
            using (var memoryStream = new MemoryStream())
            {
                await e.File.OpenReadStream(maxAllowedSize: 1024 * 1024 * 1024).CopyToAsync(memoryStream);
                musiqueModele.image = memoryStream.ToArray();
            }
        }

        private async Task LoadSong(InputFileChangeEventArgs e)
        {
            // Set the content of the image to the model
            using (var memoryStream = new MemoryStream())
            {
                await e.File.OpenReadStream(maxAllowedSize: 1024 * 1024 * 1024).CopyToAsync(memoryStream);
                musiqueModele.song = memoryStream.ToArray();
            }
        }

        protected override async Task OnInitializedAsync()
        {
            var musique = await DataService.GetById(id);           

            // Set the model with the item
            musiqueModele = MusiqueFactory.ToModel(musique,musiqueModele);
        }
    }
}
