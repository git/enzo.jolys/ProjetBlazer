﻿using Blazorise.DataGrid;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Localization;
using ProjetBlazor.Modeles;
using ProjetBlazor.Services;

namespace ProjetBlazor.Pages
{
    public partial class Play
    {
        private List<Musique> listeMusique = new List<Musique>();

        [Inject]
        public IDataService DataService { get; set; }

        [Inject]
        public IStringLocalizer<MusiquesListe> Localizer { get; set; }


        /*
        private async Task OnReadData(DataGridReadDataEventArgs<Musique> e)
        {
            if (e.CancellationToken.IsCancellationRequested)
            {
                return;
            }
            if (!e.CancellationToken.IsCancellationRequested)
            {
                listeMusique = await DataService.ListAll();
            }
        }*/


        protected override async Task OnInitializedAsync()
        {
            listeMusique = await DataService.ListAll();
        }
    }
}
