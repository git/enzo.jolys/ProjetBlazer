﻿using Blazored.Modal;
using Blazored.Modal.Services;
using Blazorise.DataGrid;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using ProjetBlazor.Modals;
using ProjetBlazor.Modeles;
using ProjetBlazor.Services;

namespace ProjetBlazor.Pages
{
    public partial class MusiquesListe
    {
        private List<Musique> listeMusique ;

        private int totalItem;

        [Inject]
        public IDataService DataService { get; set; }

        [Inject]
        public IWebHostEnvironment WebHostEnvironment { get; set; }

        [Inject]
        public IStringLocalizer<MusiquesListe> Localizer { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [CascadingParameter]
        public IModalService Modal { get; set; }

        private async Task OnReadData(DataGridReadDataEventArgs<Musique> e)
        {
            if (e.CancellationToken.IsCancellationRequested)
            {
                return;
            }
            if (!e.CancellationToken.IsCancellationRequested)
            {
                listeMusique = await DataService.List(e.Page, e.PageSize);
                totalItem = await DataService.Count();
            }
        }


        private async void OnDelete(int id)
        {
            var parameters = new ModalParameters();
            parameters.Add(nameof(Musique.id), id);

            var modal = Modal.Show<DeleteConfirmation>("Delete Confirmation", parameters);
            var result = await modal.Result;

            if (result.Cancelled)
            {
                return;
            }

            await DataService.Delete(id);

            // Reload the page
            NavigationManager.NavigateTo("MusiquesListe", true);
        }

    }

}
