﻿using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components;
using ProjetBlazor.Services;
using ProjetBlazor.Modeles;
using System.Text;

namespace ProjetBlazor.Pages
{
    public partial class AddMusique
    {

            private MusiqueModel musiqueModel = new MusiqueModel();

            [Inject]
            public IDataService DataService { get; set; }

            [Inject]
            public NavigationManager NavigationManager { get; set; }


            private async void HandleValidSubmit()
            {
                await DataService.Add(musiqueModel);

                string fileName = @$"{Environment.CurrentDirectory}/wwwroot/song/" + musiqueModel.songName;

                using (FileStream fs = File.Create(fileName))
                {                    
                    byte[] info = musiqueModel.song;
                    fs.Write(info, 0, info.Length);
                }

            NavigationManager.NavigateTo("MusiquesListe");
            }


            private async Task LoadImage(InputFileChangeEventArgs e)
            {
                
                // Set the content of the image to the model
                using (var memoryStream = new MemoryStream())
                {
                    await e.File.OpenReadStream(maxAllowedSize: 1024 * 1024 * 1024).CopyToAsync(memoryStream);
                    musiqueModel.image = memoryStream.ToArray();
                }
            }

            private async Task LoadSong(InputFileChangeEventArgs e)
            {
                // Set the content of the image to the model
                using (var memoryStream = new MemoryStream())
                {
                    await e.File.OpenReadStream(maxAllowedSize: 1024 * 1024 * 1024).CopyToAsync(memoryStream);
                    musiqueModel.song = memoryStream.ToArray();
                }
            }
    }
}
