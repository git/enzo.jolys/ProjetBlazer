﻿using System.ComponentModel.DataAnnotations;


namespace ProjetBlazor.Modeles
{
    public class MusiqueModel
    {
        
        public int id { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Le titre affiché ne doit pas dépasser 100 caractères.")]
        public string titre { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "L'auteur affiché ne doit pas dépasser 50 caractères.")]
        public string auteur { get; set; }

        [Required]
        [Range(1,3600)]
        public int duree { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Le genre affiché ne doit pas dépasser 100 caractères.")]
        public string genre { get; set; }

        [Required(ErrorMessage = "La date de la musique est obligatoire !!")]
        public DateTime date { get; set; }


        [Required(ErrorMessage = "L'image de la musique est obligatoire !!")]
        public byte[] image { get; set; }

        
        public byte[] song { get; set; }
       
        public string ImageBase64 { get; set; }

        public string songName { get; set; }
    }
}
