﻿namespace ProjetBlazor.Modeles
{
    public class Musique
    {
        public int id { get; set; }
        public string titre { get; set; }
        public string auteur { get; set; }
        public int duree { get; set; }
        public string genre { get; set; }
        public DateTime date { get; set; }

        public string ImageBase64 { get; set; }
        
        public string songName { get; set; }
    }
}
