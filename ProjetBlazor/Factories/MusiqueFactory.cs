﻿using ProjetBlazor.Modeles;

namespace ProjetBlazor.Factories
{
    public static class MusiqueFactory
    {
        public static MusiqueModel ToModel(Musique musique,MusiqueModel musiqueModele)
        {
            return new MusiqueModel
            {
                id = musique.id,
                titre = musique.titre,
                auteur = musique.auteur,
                genre = musique.genre,
                duree = musique.duree,
                date = musique.date,
                image = Convert.FromBase64String(musique.ImageBase64),
                ImageBase64 = musique.ImageBase64,
                songName= musique.songName,
            };
        }

        public static Musique Create(MusiqueModel musique)
        {
            return new Musique
            {
                id = musique.id,
                titre = musique.titre,
                auteur = musique.auteur,
                duree = musique.duree,
                genre = musique.genre,
                date = musique.date,
                songName= musique.songName,
            };
        }
    }
}
