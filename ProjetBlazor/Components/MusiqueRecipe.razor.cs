﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using ProjetBlazor.Modeles;
using ProjetBlazor.Pages;

namespace ProjetBlazor.Components
{
    public partial class MusiqueRecipe
    {
        [Parameter]
        public Musique musique { get; set; }

        [CascadingParameter]
        public DragAndDrop Parent { get; set; }

        [Inject]
        public IStringLocalizer<MusiquesListe> Localizer { get; set; }

        internal void OnDragEnter()
        {
            
        }

        internal void OnDragLeave()
        {

        }

        internal void OnDrop()
        {

            this.musique = Parent.CurrentDragMusique;


        }

        private void OnDragStart()
        {
            Parent.CurrentDragMusique = this.musique;
        }

        private void Reset()
        {
            this.musique = null;
        }
    }
}
