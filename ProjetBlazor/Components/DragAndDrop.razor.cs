﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using ProjetBlazor.Modeles;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace ProjetBlazor.Components
{
    public partial class DragAndDrop
    {
        [Parameter]
        public Musique Recipe { get; set; }

        public Musique CurrentDragMusique { get; set; }

        [Parameter]
        public List<Musique> ListMusique { get; set; }

    }
}
