﻿using Blazorise;
using Microsoft.AspNetCore.Components;
using ProjetBlazor.Modeles;

namespace ProjetBlazor.Components
{
    public partial class MusiqueComponent
    {
        [Parameter]
        public Musique musique { get; set; }

        [Parameter]
        public bool NoDrop { get; set; }

        [CascadingParameter]
        public DragAndDrop Parent { get; set; }

        internal void OnDragEnter()
        {
            if (NoDrop)
            {
                return;
            }
        }

        internal void OnDragLeave()
        {
            if (NoDrop)
            {
                return;
            }
        }

        internal void OnDrop()
        {
            if (NoDrop)
            {
                return;
            }

            this.musique = Parent.CurrentDragMusique;
        }

        private void OnDragStart()
        {
            Parent.CurrentDragMusique = this.musique;
        }
    }
}
